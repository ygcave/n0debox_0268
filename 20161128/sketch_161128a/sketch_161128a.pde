int totalNum = 20;
int cols = 10;
int rows;
float maxAngle = 0.0;
float angleSpeed = 1.0;
boolean mode = true;

void setup() {
  size(800, 800);
  background(0);
  rows = cols;
}

void draw() {  
  background(0);

  for (int y=0; y < rows; y++) {
    for (int x = 0; x < cols; x++) {
      for (int i=0; i < totalNum; i++) {
        pushMatrix();
        translate(x*(width/cols)+(width/cols)/2, y*(height/rows)+(height/rows)/2);
        float angle = 0;
        if (y%2 == 0) {
          if (x%2 == 0) {
            angle = map(i, 0, totalNum-1, 0, maxAngle);
          } else {
            angle = 360-map(i, 0, totalNum-1, 0, maxAngle);
          }
        } else if (y%2 == 1) {
          if (x%2 == 0) {
            angle = 360-map(i, 0, totalNum-1, 0, maxAngle);
          } else {
            angle = map(i, 0, totalNum-1, 0, maxAngle);
          }
        }       
        rotate(radians(angle));

        if (mode == true) {
          stroke(255);
          noFill();
        } else {
          noStroke();
          float c = map(i, 0, totalNum-1, 0, 255);
          fill(c);
        }
        float w = map(i, 0, totalNum-1, width/cols, 10);
        rect(-w/2.0, -w/2.0, w, w);
        //ellipse(200, 0, 50, 50);  
        //stroke(255, 255, 0);
        //line(0, -height/2, 0, height/2);
        //line(-width/2, 0, width/2, 0);
        popMatrix();
      }
    }
  }

  maxAngle = maxAngle + angleSpeed;
  if (maxAngle > 360) {
    maxAngle = 0;
  }
}


void keyPressed() {
  if (key == ' ') {
    saveFrame("myWork_####.tiff");
  }
  if (key == 'm') {
    mode = !mode;
  }
}