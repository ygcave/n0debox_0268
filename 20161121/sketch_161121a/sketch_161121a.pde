int count = 0;
float x = 0.0;
float w = 0.0;
float t = 0.0;
float y = 0.0;

int num = 5;

boolean b = false;

void setup() {
  size(600, 600);
  background(255, 255, 0);
}

void draw() {
  background(255, 255, 0);

  println("hello world : " + count);

  float w1 = width/num;

  noStroke();
  for (int j=0; j<num; j++) {
    for (int i=0; i<num; i=i+1) {
      fill(map(i, 0, num-1, 0, 255), map(j, 0, num-1, 0, 255), 0);
      rect(i*w1, j*w1, w1, w1);

      for (int k=0; k<5; k++) {
        if (b == true) {
          stroke(0);
        } else {
          noStroke();
        }
        fill(map(i, 0, num-1, 0, 255), map(j, 0, num-1, 0, 255), k*(255/5.0));
        ellipse(i*w1+w1/2, j*w1+w1/2, w1/(k+1), w1/(k+1));
      }
    }
  }

  fill(255, 255, 255);
  stroke(0);
  //rect(mouseX-50, mouseY-50, 100, 100);
  ellipse(x, y, w, w);

  if (x > 600) {
    x = 0.0;
  } else {
    x = x + 1.0;
  }

  w = 30+cos(t)*30;
  y = height/2 + sin(t)*200;

  t = t+0.032;
}

void keyPressed() {
  if (key == ' ') {
    b = !b;
  }
}