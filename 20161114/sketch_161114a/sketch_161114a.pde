int count = 0;
float x = 0.0;
float w = 0.0;
float t = 0.0;
float y = 0.0;

void setup(){
  size(600, 600);
  background(255, 255, 0);
}

void draw(){
  background(255, 255, 0);
  
  println("hello world : " + count);
  
  fill(255, 255, 255);
  stroke(0);
  //rect(mouseX-50, mouseY-50, 100, 100);
  ellipse(x, y, w, w);
  
  if(x > 600){
    x = 0.0;
  }else{
    x = x + 1.0;
  }
  
  w = 30+cos(t)*30;
  y = height/2 + sin(t)*200;
  
  t = t+0.032;
  
  for(int i=0; i<10; i=i+1){
    rect(i*50, height/2, 50, 50);
  }
}